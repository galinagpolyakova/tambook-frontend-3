// @flow
import * as language from "../constants/languages";
export type LanguageType =
  | typeof language.EN
  | typeof language.RU
  | typeof language.ES
  | typeof language.PT;
