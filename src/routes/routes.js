import courseRoutes from "../containers/course/routes";
import generalRoutes from "../containers/general/routes";
import bookingRoutes from "../containers/booking/routes";
import profileRoutes from "../containers/profile/routes";
import authRoutes from "../containers/auth/routes";
import blogRoutes from "../containers/blog/routes";

export default [
  ...courseRoutes,
  ...generalRoutes,
  ...bookingRoutes,
  ...profileRoutes,
  ...authRoutes,
  ...blogRoutes
];
