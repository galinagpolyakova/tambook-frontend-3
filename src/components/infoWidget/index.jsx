import React, { Component } from "react";
import { Widget } from "../Sidebar";
import "./styles.css";

class infoWidget extends Component {
  render() {
    return (
      <Widget title="NEED HELP?" type={Widget.types.LIGHT}>
        <p>
          Our friendly and experienced student advisors will help you with all
          your questions
        </p>
        <div className="contact-details">
          <div className="contact-option">
            <i className="far fa-envelope" />
            INFO@TAMBOOK.COM
          </div>
        </div>
      </Widget>
    );
  }
}

export default infoWidget;
