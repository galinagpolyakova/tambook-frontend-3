// @flow
import React, { PureComponent, Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { UserSettingsConsumer } from "../../contexts/UserSettings";

import NavBar from "../../shared/components/Navbar";
import NavItem from "../../shared/components/NavItem";
import Auth from "../../containers/auth/pages/auth";
import Button from "../../shared/components/Button";
import Icon from "../../shared/components/Icon";

import UserPreferences from "../UserPreferences/UserPreferences";
import { SITE_URL } from "../../config/app";

import { authSignOut } from "../../containers/auth/store/action";

import tambookLogo from "../../assets/tambook-logo.png";

import "./styles.css";

type HeaderProps = {
  isAuthenticated: boolean,
  authSignOut: Function,
  toggleAuth: Function,
  compareList: string[],
  passport: string
};

type HeaderState = {
  showAuthModal: boolean
};

class HeaderBlock extends PureComponent<HeaderProps, HeaderState> {
  render() {
    const { isAuthenticated, compareList, passport } = this.props;
    return (
      <Fragment>
        <header className="header">
          <div className="container">
            <div className="site-logo">
              <Link to="/">
                <img src={tambookLogo} alt="Tambook Logo" />
              </Link>
            </div>
            <NavBar>
              <ul>
                <NavItem>
                  <Link to="/">Home</Link>
                </NavItem>
                <NavItem>
                  <Link to="/about-us">About Us</Link>
                </NavItem>
                <NavItem>
                  <Link to="/contact-us">Contact Us</Link>
                </NavItem>
                {/*<NavItem>
                  <Link to="/under-construction">Travel Guide</Link>
                </NavItem>
                <NavItem>
                  <Link to="/under-construction">Need Help?</Link>
                </NavItem>
                <NavItem>
                  <Link to="/under-construction">Experiences</Link>
                </NavItem>*/}
                {isAuthenticated ? (
                  <NavItem>
                    <Link to="/profile">Profile</Link>
                  </NavItem>
                ) : (
                    <Fragment />
                  )}
                <UserSettingsConsumer>
                  {({ toggleAuth, toggleUserPreference }) => (
                    <Fragment>
                      <NavItem>
                        <div
                          className="country-selector"
                          onClick={toggleUserPreference}
                        >
                          <img
                            src={`${SITE_URL}/upload/countries/${passport}.png`}
                            title="NZ"
                          />
                          {passport}
                        </div>
                      </NavItem>
                      <NavItem>
                        <Link to="/filter?country=&language=">
                          <Icon icon="search" />{" "}
                        </Link>
                      </NavItem>
                      <NavItem>
                        <div className="call-to-action">
                          {isAuthenticated ? (
                            <Button
                              className="btn"
                              onClick={this.props.authSignOut}
                              outline
                              type={Button.type.LIGHT}
                            >
                              Logout
                            </Button>
                          ) : (
                              <Button
                                className="btn"
                                onClick={toggleAuth}
                                outline
                                type={Button.type.LIGHT}
                              >
                                Login / Sign up
                            </Button>
                            )}
                        </div>
                      </NavItem>
                    </Fragment>
                  )}
                </UserSettingsConsumer>
              </ul>
            </NavBar>
          </div>
        </header>
        <Auth />
        <UserPreferences />
        {compareList.length > 1 && (
          <div className="compare-cta-button">
            <Button htmlType={Button.htmlType.LINK} link={`/compare`}>
              Compare <span>{compareList.length}</span>
            </Button>
          </div>
        )}
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    errors: state.auth.errors,
    isAuthSuccess: state.auth.isAuthSuccess,
    compareList: state.course.compareList,
    passport: state.profile.settings.passport,
    language: state.profile.settings.language
  };
}

const Actions = {
  authSignOut
};

export default connect(
  mapStateToProps,
  Actions
)(HeaderBlock);
