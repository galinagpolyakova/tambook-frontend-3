// @flow
import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";

import { type ConsolidatedCourseType } from "../../containers/course/types";

import Button from "../../shared/components/Button";
import { Rating } from "../../shared/components/Rate";
import IconButton from "../../shared/components/IconButton";
import Tooltip from "../../shared/components/Tooltip";

import { isNotEmpty } from "../../shared/utils";

import missingImage from "../../assets/missing-course.jpg";

import "./styles.css";
import Currency from "../Currency";

type CourseProps = {
  id: $PropertyType<ConsolidatedCourseType, "id">,
  title: $PropertyType<ConsolidatedCourseType, "title">,
  location: $PropertyType<ConsolidatedCourseType, "location">,
  price: $PropertyType<ConsolidatedCourseType, "price">,
  image: $PropertyType<ConsolidatedCourseType, "image">,
  rating: $PropertyType<ConsolidatedCourseType, "rating">,
  duration: $PropertyType<ConsolidatedCourseType, "duration">,
  isBookmarked: $PropertyType<ConsolidatedCourseType, "isBookmarked">,
  isAddedToCompare: $PropertyType<ConsolidatedCourseType, "isAddedToCompare">,
  currencyType: $PropertyType<ConsolidatedCourseType, "currencyType">,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function,
  addCourseToCompare: Function,
  removeCourseFromCompare: Function
};

class Course extends PureComponent<CourseProps> {
  addCourseToBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.addCourseToBookmarkList) {
      this.props.addCourseToBookmarkList(courseId);
    }
  };

  removeCourseFromBookmarkList = (
    courseId: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.removeCourseFromBookmarkList) {
      this.props.removeCourseFromBookmarkList(courseId);
    }
  };

  addCourseToCompare = (id: $PropertyType<ConsolidatedCourseType, "id">) => {
    if (this.props.addCourseToCompare) {
      this.props.addCourseToCompare({ id });
    }
  };

  removeCourseFromCompare = (
    id: $PropertyType<ConsolidatedCourseType, "id">
  ) => {
    if (this.props.removeCourseFromCompare) {
      this.props.removeCourseFromCompare({ id });
    }
  };

  render() {
    const {
      id,
      title,
      location,
      price,
      image,
      rating,
      duration,
      isBookmarked,
      isAddedToCompare,
      currencyType
    } = this.props;
    const courseImage = isNotEmpty(image) ? image : missingImage;
    return (
      <div className="course">
        <div className="featured-image">
          <img src={courseImage} alt={title} />
          <div className="hover">
            <div className="button-group">
              <Tooltip
                position={Tooltip.position.BOTTOM}
                message="Add to compare"
              >
                <IconButton
                  icon="exchange-alt"
                  className={classNames({ active: isAddedToCompare })}
                  onClick={() => {
                    isAddedToCompare
                      ? this.removeCourseFromCompare(id)
                      : this.addCourseToCompare(id);
                  }}
                />
              </Tooltip>
              <Tooltip
                position={Tooltip.position.BOTTOM}
                message="Add to bookmark"
              >
                <IconButton
                  icon="bookmark"
                  className={classNames({ active: isBookmarked })}
                  onClick={() =>
                    isBookmarked
                      ? this.removeCourseFromBookmarkList(id)
                      : this.addCourseToBookmarkList(id)
                  }
                />
              </Tooltip>
            </div>
            <div>
              <Button
                type={Button.type.SECONDARY}
                size={Button.size.SMALL}
                htmlType={Button.htmlType.LINK}
                link={`/course/${id}`}
              >
                MORE INFO
              </Button>
            </div>
          </div>
        </div>
        <div className="course-content">
          <Link to={`/course/${id}`}>
            <div className="course-title">{title}</div>
            <div className="course-duration">{duration} hours / week</div>
            <div className="location-title">
              {location.city}, {location.country}
            </div>
            <Rating rate={rating.rate} reviews={rating.reviews} />
            <span className="price">
              FROM{" "}
              <strong>
                <Currency currencyType={currencyType}>{price}</Currency>
              </strong>{" "}
              / WEEK
            </span>
          </Link>
        </div>
      </div>
    );
  }
}

export default Course;
