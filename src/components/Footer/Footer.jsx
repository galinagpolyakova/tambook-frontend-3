import React, { PureComponent, Fragment } from "react";
import { Link } from "react-router-dom";

import Row from "../../shared/components/Row";
import Col from "../../shared/components/Col";

import tambookLogo from "../../assets/tambook-logo.png";

import "./styles.css";

class Footer extends PureComponent {
  render() {
    return (
      <Fragment>
        <footer>
          <div className="container">
            <Row>
              <Col size="2">
                <h3>Contact Us</h3>
                <ul>
                  {/*<li>
                    <span>Phone:</span> +93 123 456 789
                  </li>*/}
                  <li>
                    <span>Email us:</span> office@tambook.com
                  </li>
                  <li>
                    <span>Address:</span> 3 Glenside Crescent, Eden Terrace,
                    Auckland, NZ.
                  </li>
                </ul>
              </Col>
              <Col size="4">
                <Row>
                  {/*<Col>
                    <h3>Top Languages</h3>
                    <ul>
                      <li>English</li>
                      <li>French</li>
                      <li>Spanish</li>
                      <li>German</li>
                      <li>Chinese</li>
                    </ul>
                  </Col>*/}
                  <Col>
                    {/*<h3>Popular Countries</h3>
                    <Col>
                      <ul>
                        <li>France</li>
                        <li>Spain</li>
                        <li>Ukraine</li>
                        <li>Turkey</li>
                        <li>India</li>
                      </ul>
                    </Col>*/}
                    <Col>
                      {/* <ul>
                        <li>France</li>
                        <li>Spain</li>
                        <li>Ukraine</li>
                        <li>Turkey</li>
                        <li>India</li>
                      </ul> */}
                    </Col>
                  </Col>
                </Row>
              </Col>
              <Col size="2">
                <h3>Follow us:</h3>
                <ul className="social-links">
                  <li className="facebook">
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href="https://www.facebook.com/tambook.language.courses/"
                    >
                      <i className="fa-fw fab fa-facebook-f" />
                      facebook
                    </a>
                  </li>
                  <li className="linkedin">
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href="https://www.linkedin.com/company/tambook-limited/"
                    >
                      <i className="fa-fw fab fa-linkedin" />
                      linkedin
                    </a>
                  </li>
                  <li className="instagram">
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href="https://www.instagram.com/tambook.language.courses/"
                    >
                      <i className="fa-fw fab fa-instagram" />
                      instagram
                    </a>
                  </li>
                </ul>
              </Col>
              <Col size="4">
                <h3>Resources</h3>
                <Row>
                  <Col>
                    <ul>
                      {/*<li>
                        <Link to="/under-construction">WORKING ABROAD</Link>
                      </li>*/}
                      {/*<li>
                        <Link to="/under-construction">VISA</Link>
                      </li>*/}
                      {/*<li>
                        <Link to="/under-construction">HOW IT WORKS</Link>
                      </li>*/}
                      {/*<li>
                        <Link to="/terms-and-conditions">
                          TERMS AND CONDITIONS
                        </Link>
                      </li>*/}
                      <li>
                        <a href="https://school.tambook.com/signup">
                          SIGN UP AS SCHOOL
                        </a>
                      </li>
                      <li>
                        <a href="https://school.tambook.com/login">
                          SIGN IN AS SCHOOL
                        </a>
                      </li>
                    </ul>
                  </Col>
                  <Col>
                    <ul>
                      <li>
                        <Link to="/about-us">ABOUT US</Link>
                      </li>
                      <li>
                        <Link to="/contact-us">CONTACT US</Link>
                      </li>
                      <li>
                        <Link to="/faq">FAQ</Link>
                      </li>
                      {/*<li>
                        <Link to="/under-construction">BLOG</Link>
                      </li>*/}
                      {/*<li>
                        <Link to="/under-construction">WHY STUDY ABROAD</Link>
                      </li>*/}
                      {/*<li>
                        <Link to="/under-construction">SUCCESS STORIES</Link>
                      </li>*/}
                    </ul>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </footer>
        <div className="footer-note">
          <div className="container">
            <img src={tambookLogo} alt="Tambook Logo" />
            <span>© 2019 All rights reserved. Tambook.</span>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Footer;
