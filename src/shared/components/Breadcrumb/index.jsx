// @flow
import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import "./styles.css";

type BreadcrumbType = {
  title: string,
  link: string
};

export type BreadcrumbListType = Array<BreadcrumbType>;

type BreadcrumbProps = {
  breadcrumbs: Array<{
    title: string,
    link: string
  }>
};

class Breadcrumb extends PureComponent<BreadcrumbProps> {
  render() {
    const { breadcrumbs } = this.props;
    return (
      <ul className="breadcrumb">
        <li><Link to="/">HOME</Link></li>
        {breadcrumbs.map(({ title, link }) => (
          <li key={title}>{link ? <Link to={link}>{title}</Link> : title}</li>
        ))}
      </ul>
    );
  }
}

export default Breadcrumb;
