// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

type StarProps = {
  isFilled: boolean,
  isHalf: boolean
};

class Star extends PureComponent<StarProps> {
  render() {
    const { isFilled, isHalf } = this.props;
    return (
      <div
        className={classNames(
          "rate-star",
          {
            "rate-star-full": isFilled && !isHalf
          },
          {
            "rate-star-half": isFilled && isHalf
          }
        )}
      >
        <div className="unfilled-star">
          <i className="fas fa-star" />
        </div>
        <div className="filled-star">
          <i className="fas fa-star" />
        </div>
      </div>
    );
  }
}

export default Star;
