// @flow
import type { Element } from "react";
import React, { PureComponent } from "react";
import classNames from "classnames";

type TabPaneProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  isActive: boolean
};

class TabPane extends PureComponent<TabPaneProps> {
  render() {
    const { children, isActive } = this.props;
    return (
      <div className={classNames("tab-pane", { active: isActive })}>
        {children}
      </div>
    );
  }
}

export default TabPane;
