// @flow
import type { IconLibraryType } from "./Icon";

import React, { PureComponent } from "react";

import Icon from "./Icon";

export type IconButtonProps = {
  icon: string,
  iconlibrary?: IconLibraryType,
  className?: string,
  onClick?: Function
};

export default class IconButton extends PureComponent<IconButtonProps> {
  handleClick(e: SyntheticEvent<HTMLElement>) {
    if (this.props.onClick !== undefined) {
      this.props.onClick(e);
    }
  }
  render() {
    const { className, icon, iconlibrary } = this.props;
    return (
      <button onClick={this.handleClick.bind(this)} className={className}>
        <Icon icon={icon} library={iconlibrary} fixedWidth />
      </button>
    );
  }
}
