// @flow
import React, { PureComponent } from "react";

type SliderControlsProps = {
  slides: Array<string>,
  handleClick: Function
};

class SliderControls extends PureComponent<SliderControlsProps> {
  render() {
    return (
      <div className="slider-controls">
        {this.props.slides.map((image, i) => (
          <div
            className="control"
            key={i}
            onClick={() => this.props.handleClick(i)}
          >
            <img alt={i} src={image} />
          </div>
        ))}
      </div>
    );
  }
}

export default SliderControls;
