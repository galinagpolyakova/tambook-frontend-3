// @flow
import type { Element } from "react";

import React, { PureComponent } from "react";
import classNames from "classnames";

import "./styles.css";

type ColProps = {
  children: | Array<Element<any> | string | typeof undefined>
    | Element<any>
    | string
    | typeof undefined,
  size?: string,
  styles?: string,
  className?: string
};
class Col extends PureComponent<ColProps> {
  render() {
    const { size, className, styles } = this.props;
    const colClass = size ? `col-${size}` : "";
    return (
      <div className={classNames("col", className, colClass)} style={styles}>
        {this.props.children}
      </div>
    );
  }
}

export default Col;
