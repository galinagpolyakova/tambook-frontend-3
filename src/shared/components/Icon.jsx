// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

export type IconLibraryType =
  | typeof Icon.FONTAWESOME_REGULAR
  | typeof Icon.FONTAWESOME_SOLID
  | typeof Icon.FONTAWESOME_LIGHT
  | typeof Icon.FONTAWESOME_BRAND;

export type IconProps = {
  icon: string,
  library: IconLibraryType,
  fixedWidth: boolean,
  className: string
};

class Icon extends PureComponent<IconProps> {
  static FONTAWESOME_REGULAR = "far";
  static FONTAWESOME_SOLID = "fas";
  static FONTAWESOME_LIGHT = "fal";
  static FONTAWESOME_BRAND = "fab";
  static defaultProps = {
    library: Icon.FONTAWESOME_SOLID,
    fixedWidth: false,
    className: ""
  };
  render() {
    const { library, icon, fixedWidth, className } = this.props;
    return (
      <i
        className={classNames(
          library,
          `fa-${icon}`,
          { "fa-fw": fixedWidth },
          className
        )}
      />
    );
  }
}

export default Icon;
