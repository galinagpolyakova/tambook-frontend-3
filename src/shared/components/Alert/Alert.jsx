// @flow
import React, { PureComponent } from "react";
import classNames from "classnames";

import "./styles.css";

type AlertProps = {
  isFullWidth: boolean,
  children: any,
  type: | typeof Alert.type.SUCCSESS
    | typeof Alert.type.ERROR
    | typeof Alert.type.INFO
};

class Alert extends PureComponent<AlertProps> {
  static type = {
    SUCCSESS: "alert-success",
    ERROR: "alert-danger",
    LIGHT: "alert-light",
    INFO: "alert-info"
  };
  static defaultProps = {
    type: Alert.type.INFO,
    isFullWidth: false
  };
  render() {
    const { children, type, isFullWidth } = this.props;
    return (
      <div className={classNames("alert", { "full-width": isFullWidth }, type)}>
        {children}
      </div>
    );
  }
}

export default Alert;
