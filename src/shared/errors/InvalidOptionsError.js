// @flow
export default class InvalidOptionsError extends Error {
    constructor() {
        super("Options are invalid");
    }
}
