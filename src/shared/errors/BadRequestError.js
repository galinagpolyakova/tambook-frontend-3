// @flow
import type { NetworkErrorInterface } from "./NetworkErrorInterface";

export default class BadRequestError extends Error implements NetworkErrorInterface<any> {
    _response: any;

    constructor(response: any) {
        super('Bad Request');
        this._response = response;
    }

    getResponse(): any {
        return this._response;
    }
}
