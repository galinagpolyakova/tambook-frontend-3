import toDate from "./toDate";

export default function eachDay(dirtyStartDate, dirtyEndDate, dirtyStep) {
  const startDate = toDate(dirtyStartDate);
  const endDate = toDate(dirtyEndDate);
  const step = dirtyStep !== undefined ? dirtyStep : 1;

  const endTime = endDate.getTime();

  if (startDate.getTime() > endTime) {
    throw new Error("The first date cannot be after the second date");
  }

  const dates = [];

  const currentDate = startDate;
  currentDate.setHours(0, 0, 0, 0);

  while (currentDate.getTime() <= endTime) {
    dates.push(toDate(currentDate));
    currentDate.setDate(currentDate.getDate() + step);
  }

  return dates;
}
