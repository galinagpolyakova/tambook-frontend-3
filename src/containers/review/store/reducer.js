// @flow
import type { Action } from "../../../shared/types/ReducerAction";
import {
  GET_FEATURED_REVIEWS_INIT,
  GET_FEATURED_REVIEWS_SUCCESS,
  GET_FEATURED_REVIEWS_FAILURE
} from "./actionTypes";

export type ReviewState = {
  submitting: boolean,
  loading: boolean,
  featuredReviews: Array<any>
};

const initialState: ReviewState = {
  submitting: false,
  loading: false,
  featuredReviews: []
};

const reducer = (
  state: ReviewState = initialState,
  { type, payload = {} }: Action
) => {
  switch (type) {
    case GET_FEATURED_REVIEWS_INIT:
      return getFeaturedReviewsInit(state);
    case GET_FEATURED_REVIEWS_SUCCESS:
      return getFeaturedReviewsSuccess(state, payload);
    case GET_FEATURED_REVIEWS_FAILURE:
      return getFeaturedReviewsFailure(state, payload);
    default:
      return state;
  }
};

function getFeaturedReviewsInit(state) {
  return {
    ...state,
    loading: true
  };
}

function getFeaturedReviewsSuccess(state, { featuredReviews }) {
  return {
    ...state,
    loading: false,
    featuredReviews
  };
}

function getFeaturedReviewsFailure(state, { notifications }) {
  return {
    ...state,
    notifications,
    loading: false
  };
}

export default reducer;
