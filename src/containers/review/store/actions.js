import {
  GET_FEATURED_REVIEWS_INIT,
  GET_FEATURED_REVIEWS_SUCCESS,
  GET_FEATURED_REVIEWS_FAILURE
} from "./actionTypes";

function getFeaturedReviewsInit() {
  return {
    type: GET_FEATURED_REVIEWS_INIT
  };
}

function getFeaturedReviewsSuccess(payload) {
  return {
    type: GET_FEATURED_REVIEWS_SUCCESS,
    payload
  };
}

function getFeaturedReviewsFailure(payload) {
  return {
    type: GET_FEATURED_REVIEWS_FAILURE,
    payload
  };
}

export function getFeaturedReviews() {
  return (dispatch, getState, serviceManager) => {
    dispatch(getFeaturedReviewsInit());

    let courseService = serviceManager.get("ReviewService");

    courseService
      .getFeaturedReviews()
      .then(data =>
        dispatch(
          getFeaturedReviewsSuccess({
            featuredReviews: data
          })
        )
      )
      .catch(err => {
        dispatch(getFeaturedReviewsFailure(err));
      });
  };
}
