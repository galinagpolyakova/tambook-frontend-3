// import lib
import Loadable from "react-loadable";

// import components
import LoadingComponent from "../../components/Loader";

export default [
  {
    path: "/profile/",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/Bookings"),
      loading: LoadingComponent
    })
  },
  {
    path: "/profile/bookings",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/Bookings"),
      loading: LoadingComponent
    })
  },
  {
    path: "/profile/bookings/:id",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/Booking"),
      loading: LoadingComponent
    })
  },
  {
    path: "/profile/bookmarks",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/Bookmarks"),
      loading: LoadingComponent
    })
  },
  {
    path: "/profile/user-information",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/UserInformation"),
      loading: LoadingComponent
    })
  },
  {
    path: "/profile/payment-method",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/PaymentMethod"),
      loading: LoadingComponent
    })
  },
  {
    path: "/profile/notifications",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/Notifications"),
      loading: LoadingComponent
    })
  }
];
