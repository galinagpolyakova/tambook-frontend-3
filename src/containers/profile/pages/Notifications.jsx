import React, { Component } from "react";
import Layout from "../components/layout";
import Alert from "../../../shared/components/Alert/Alert";
import Button from "../../../shared/components/Button";

class Notifications extends Component {
  render() {
    const notifications = [
      {
        hasRead: false,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: false,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: false,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: false,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: true,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: true,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: true,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: true,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      },
      {
        hasRead: true,
        message:
          'The order "Certificate III Business at Lexis English SUNSHINE COAST in Sunshine Coast, Australia" has changed status from "Tambook verified" to "Approved" 18.05.2018'
      }
    ];
    return (
      <Layout currentLink="notifications" currentTitle="Notifications">
        <div className="notifications">
          <div className="notifications-header">
            <Button type={Button.type.PRIMARY}>Mark all as read</Button>
            <Button>Notifications settings</Button>
          </div>
          <div className="notifications-body">
            {notifications.map(({ message, hasRead }, index) => (
              <Alert
                key={index}
                type={hasRead ? Alert.type.LIGHT : Alert.type.INFO}
              >
                {message}
              </Alert>
            ))}
          </div>
        </div>
      </Layout>
    );
  }
}

export default Notifications;
