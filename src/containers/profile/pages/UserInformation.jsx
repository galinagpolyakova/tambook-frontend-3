// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import { getUserProfile, updateProfile } from "../store/actions";

import Layout from "../components/layout";
import EditProfile from "../components/EditProfile";

type UserInformationProps = {
  getUserProfile: Function,
  user: Object,
  updateProfile: Function
};

type UserInformationState = {
  isOnEdit: boolean
};

class UserInformation extends Component<
  UserInformationProps,
  UserInformationState
> {
  state = {
    isOnEdit: false
  };
  componentDidMount() {
    this.props.getUserProfile();
  }

  toggleEdit() {
    this.setState({ isOnEdit: true });
  }
  updateProfile(payload) {
    this.props.updateProfile(payload);
    this.setState({ isOnEdit: false });
  }
  render() {
    const { isOnEdit } = this.state;
    const { user } = this.props;
    return (
      <Layout currentLink="user-information" currentTitle="User information">
        <div className="user-information">
          {user !== null && (
            <EditProfile
              {...user}
              isOnEdit={isOnEdit}
              toggleEdit={this.toggleEdit.bind(this)}
              onCompleteEditing={this.updateProfile.bind(this)}
            />
          )}
        </div>
      </Layout>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.profile.userProfile
  };
}

const Actions = {
  getUserProfile,
  updateProfile
};

export default connect(
  mapStateToProps,
  Actions
)(UserInformation);
