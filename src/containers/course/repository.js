// @flow
import { Repository } from "../../shared/Repository";
import { Course } from "./model";
import type { CourseType } from "./types";

export class CourseRepository extends Repository<Course, CourseType> {
  _comparator(a: Course, b: Course): boolean {
    return a.id === b.id;
  }
}
