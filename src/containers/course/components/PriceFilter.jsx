// @flow
import React, { PureComponent, Fragment } from "react";
import Slider, { createSliderWithTooltip } from "rc-slider";
import Button from "../../../shared/components/Button";

const Range = createSliderWithTooltip(Slider.Range);

type PriceFilterProps = {
  searchCourses: Function,
  selectedMinPrice: number,
  selectedMaxPrice: number,
  minPrice: number,
  maxPrice: number
};
type PriceFilterState = {
  minPrice: number,
  maxPrice: number
};

class PriceFilter extends PureComponent<PriceFilterProps, PriceFilterState> {
  static defaultProps = {
    minPrice: 0,
    maxPrice: 1500
  };
  state = {
    minPrice: 0,
    maxPrice: 1500
  };
  onChange = (value: Array<number>) => {
    const [minPrice, maxPrice] = value;
    this.setState({
      minPrice,
      maxPrice
    });
  };

  filterByPrice = () => {
    const { minPrice, maxPrice } = this.state;
    this.props.searchCourses({ minPrice, maxPrice });
  };

  componentDidMount() {
    const { selectedMinPrice, selectedMaxPrice } = this.props;
    if (selectedMaxPrice !== undefined && selectedMinPrice !== undefined) {
      this.setState({
        minPrice: parseInt(selectedMinPrice),
        maxPrice: parseInt(selectedMaxPrice)
      });
    } else if (selectedMaxPrice !== undefined) {
      this.setState({
        maxPrice: parseInt(selectedMaxPrice)
      });
    } else if (selectedMinPrice !== undefined) {
      this.setState({
        minPrice: parseInt(selectedMinPrice)
      });
    }
  }
  render() {
    const { minPrice, maxPrice } = this.state;
    return (
      <Fragment>
        <div className="price-slider">
          <Range
            min={this.props.minPrice}
            max={this.props.maxPrice}
            value={[minPrice, maxPrice]}
            onChange={this.onChange}
          />
        </div>
        <div className="price-range">
          <span className="price-min">${this.props.minPrice}</span>
          <span className="price-max">${this.props.maxPrice}</span>
        </div>
        <Button
          size={Button.size.SMALL}
          type={Button.type.PRIMARY}
          onClick={this.filterByPrice}
        >
          Filter
        </Button>
      </Fragment>
    );
  }
}

export default PriceFilter;
