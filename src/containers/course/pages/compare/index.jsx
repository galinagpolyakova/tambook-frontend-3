// @flow
import React, { Component } from "react";
import { connect } from "react-redux";

import {
  getComparedCourseList,
  removeCourseFromCompare
} from "../../store/actions";

import "./styles.css";
import IconButton from "../../../../shared/components/IconButton";
import PageHeader from "../../../../components/PageHeader";
import Alert from "../../../../shared/components/Alert/Alert";
import Section from "../../../../shared/components/Section";

type CompareCoursesProps = {
  list: string[],
  compareListCourses: Object[],
  getComparedCourseList: Function,
  removeCourseFromCompare: Function
};

class CompareCourses extends Component<CompareCoursesProps> {
  componentDidMount() {
    if (this.props.list.length > 0) {
      this.props.getComparedCourseList({
        list: this.props.list
      });
    }
  }
  componentDidUpdate(prevProps) {
    if (this.props.list !== prevProps.list && this.props.list.length > 0) {
      this.props.getComparedCourseList({
        list: this.props.list
      });
    }
  }
  render() {
    return (
      <div>
        <PageHeader
          title="Compare Courses"
          breadcrumbs={[
            {
              title: "COMPARE COURSES"
            }
          ]}
        />
        <div className="container">
          {this.props.list.length === 0 ? (
            <Section>
              <Alert>No courses in compare list</Alert>
            </Section>
          ) : (
            <section className="courses-comparison-table">
              <div className="courses-compare-table">
                <div className="features">
                  <div className="top-info" />
                  <div className="features-list">
                    <div>Price</div>
                    <div>Destination</div>
                    <div>Intensity</div>
                    <div>Duration</div>
                    <div>Ability to work</div>
                    <div>Course type</div>
                  </div>
                </div>
                <div className="courses-wrapper">
                  <div
                    className="courses-columns"
                    style={{
                      width: `${this.props.compareListCourses.length * 310}px`
                    }}
                  >
                    {this.props.compareListCourses.map(
                      ({
                        id,
                        image,
                        title,
                        price,
                        location,
                        intensity,
                        duration,
                        eligibleToWork,
                        type
                      }) => (
                        <div className="course-item" key={id}>
                          <div className="top-info">
                            <IconButton
                              icon="times-circle"
                              className="close"
                              onClick={() =>
                                this.props.removeCourseFromCompare({ id })
                              }
                            />
                            <img src={image} alt={title} />
                            <h3>{title}</h3>
                          </div>
                          <div className="features-list">
                            <div>{price}</div>
                            <div>{location}</div>
                            <div>{intensity}</div>
                            <div>{duration}</div>
                            <div>{eligibleToWork ? "Yes" : "No"}</div>
                            <div>{type}</div>
                          </div>
                        </div>
                      )
                    )}
                  </div>
                </div>
              </div>
            </section>
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    list: state.course.compareList,
    compareListCourses: state.course.compareListCourse
  };
}

const Actions = {
  getComparedCourseList,
  removeCourseFromCompare
};

export default connect(
  mapStateToProps,
  Actions
)(CompareCourses);
