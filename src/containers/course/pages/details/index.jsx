// @flow
import { type CourseType } from "../../types";

import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import PageHeader from "../../../../components/PageHeader";
import ImageSlider from "../../../../shared/components/ImageSlider";
import Rate, { Rating } from "../../../../shared/components/Rate";
import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import CourseSidebar from "../../components/CourseSidebar";
import Review from "../../../../components/Review";
import RelatedCources from "../../components/RelatedCources";
import Section, {
  SectionBody,
  SectionHeader
} from "../../../../shared/components/Section";
import Loader from "../../../../components/Loader";

import {
  addCourseToBookmarkList,
  getCourse,
  getBookmarkCourseList,
  removeCourseFromBookmarkList
} from "../../store/actions";
import {
  isCourseLoaded,
  getCourseId,
  getConsolidatedCourse,
  isCourseBookmarksListLoaded
} from "../../store/selectors";
import { isAuthenticated } from "../../../auth/store/selectors";
import { UserSettingsConsumer } from "../../../../contexts/UserSettings";

import "./styles.css";
import FaqSection from "../../components/FaqSection";
import CourseInformation from "../../components/CourseInformation";
import Currency from "../../../../components/Currency";
import Alert from "../../../../shared/components/Alert";

export type CourseDetailsProps = {
  getCourse: Function,
  addCourseToBookmarkList: Function,
  course: CourseType,
  courseId: number,
  isLoaded: boolean,
  isBookmarkLoading: boolean,
  isAuthenticated: boolean,
  isBookmarksListLoaded: boolean,
  isCourseBookmarked: boolean,
  match: {
    params: {
      courseId: number
    }
  },
  getBookmarkCourseList: Function,
  removeCourseFromBookmarkList: Function,
  toggleAuth: Function,
  history: any,
  selectedCurrency: string,
  passport: string
};

class CourseDetails extends PureComponent<CourseDetailsProps> {
  componentDidMount() {
    const {
      match: {
        params: { courseId }
      }
    } = this.props;
    !this.props.isLoaded ? this.props.getCourse(courseId) : "";
    this.props.isAuthenticated ? this.props.getBookmarkCourseList() : "";
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.courseId !== this.props.match.params.courseId) {
      this.props.getCourse(this.props.match.params.courseId);
    }
    if (this.props.isAuthenticated && !this.props.isBookmarksListLoaded) {
      this.props.getBookmarkCourseList();
    }
  }

  addCourseToBookmarkList = () => {
    const { courseId } = this.props;
    if (this.props.isAuthenticated) {
      this.props.addCourseToBookmarkList(courseId);
    } else {
      this.props.toggleAuth();
    }
  };

  removeCourseFromBookmarkList = () => {
    const { courseId } = this.props;
    this.props.removeCourseFromBookmarkList(courseId);
  };

  bookACourse = (selectedWeek, selectedRange) => {
    const { courseId } = this.props;
    if (this.props.isAuthenticated) {
      this.props.history.push(
        `/booking/course/${courseId}?selectedWeek=${selectedWeek}&selectedRange=${selectedRange}`
      );
    } else {
      this.props.toggleAuth();
    }
  };

  render() {
    const { course, isLoaded, selectedCurrency, passport, isBookmarkLoading } = this.props;
    const {
      match: {
        params: { courseId }
      }
    } = this.props;

    if (!isLoaded) {
      return <Loader isLoading />;
    }

    return (
      <div>
        <PageHeader
          title={course.title}
          breadcrumbs={[
            {
              title: "COURSES",
              link: "/filter"
            },
            {
              title: "DETAIL"
            }
          ]}
        />
        <div className="container">
          <div className="course-header">
            <Row fullWidth>
              <Col size="8">
                {course.intensity !== null && (
                  <span className="course-duration">
                    FULL TIME: {course.intensity}
                    HR/WK
                  </span>
                )}
                <p className="title">{course.schoolName}</p>
                <Rating
                  rate={course.rating.rate}
                  reviews={course.rating.reviews}
                />
              </Col>
              <Col size="4">
                <div className="price">
                  PRICE FROM{" "}
                  <span>
                    <Currency currencyType={course.currencyType}>
                      {course.startingPrice}
                    </Currency>
                  </span>
                  /WEEK
                </div>
              </Col>
            </Row>
          </div>
          <div className="course-content">
            <Row>
              <Col size="8">
                <Section className="course-images">
                  <SectionBody>
                    <ImageSlider autoplay slides={course.images} />
                  </SectionBody>
                </Section>
                <CourseInformation information={course.information} />
                {course.youtubeLink ? (
                  <Section className="promo-video">
                    <iframe
                      title="school-promo"
                      width="100%"
                      height="450"
                      src={course.youtubeLink}
                      allow="autoplay; encrypted-media"
                    />
                  </Section>
                ) : (
                    ""
                  )}
                <FaqSection country={course.location.country} />
                <Section className="reviews-section">
                  <SectionHeader
                    position="left"
                    heading={
                      <Fragment>
                        <span>REVIEWS</span>
                        <Rate value={course.rating.rate} />
                      </Fragment>
                    }
                  />
                  <SectionBody>
                    {course.reviews.length === 0 ? (
                      <Alert>No reviews</Alert>
                    ) : (
                        course.reviews.map((review, index) => (
                          <Review
                            key={index}
                            name={review.student}
                            rating={review.rating}
                            image={review.image}
                            comment={review.comment}
                          />
                        ))
                      )}
                  </SectionBody>
                </Section>
              </Col>
              <Col size="4">
                <CourseSidebar
                  courseId={courseId}
                  rating={course.rating.rate}
                  location={course.location}
                  courseFees={course.courseFees}
                  workEligibility={course.workEligibility}
                  passport={passport}
                  language={course.language}
                  age={course.minAge}
                  price={course.startingPrice}
                  googleMap={course.googleMap}
                  homestays={course.homestays}
                  discountList={course.discountList}
                  isBookmarked={course.isBookmarked}
                  onAddToList={this.addCourseToBookmarkList}
                  enrolmentFee={course.enrolmentFee}
                  onRemoveFromList={this.removeCourseFromBookmarkList}
                  bookACourse={this.bookACourse}
                  selectedCurrency={selectedCurrency}
                  isPaymentEnabled={course.isPaymentEnabled}
                  currencyType={course.currencyType}
                  googleCordinates={course.googleCordinates}
                  isBookmarkLoading={isBookmarkLoading}
                />
              </Col>
            </Row>
            {isLoaded ? (
              <RelatedCources
                country={course.location.country}
                language={course.language}
                addCourseToBookmarkList={this.addCourseToBookmarkList}
                removeCourseFromBookmarkList={this.removeCourseFromBookmarkList}
              />
            ) : (
                ""
              )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, props) => {
  // eslint-disable-next-line no-console
  console.log(props);
  return {
    course: getConsolidatedCourse(state),
    courseId: getCourseId(state, props),
    isLoaded: isCourseLoaded(state, props),
    isAuthenticated: isAuthenticated(state),
    isBookmarksListLoaded: isCourseBookmarksListLoaded(state),
    isBookmarkLoading: state.course.isBookmarkLoading,
    selectedCurrency: state.profile.settings.currency,
    passport: state.profile.settings.passport
  };
};

const Actions = {
  getCourse,
  addCourseToBookmarkList,
  getBookmarkCourseList,
  removeCourseFromBookmarkList
};

const CourseDetailsContainer = connect(
  mapStateToProps,
  Actions
  // $FlowFixMe
)(withRouter(CourseDetails));

export type CourseDetailsContextProps = {
  getCourse: Function,
  addCourseToBookmarkList: Function,
  course: CourseType,
  courseId: number,
  isLoaded: boolean,
  isAuthenticated: boolean,
  isBookmarksListLoaded: boolean,
  isCourseBookmarked: boolean,
  match: {
    params: {
      courseId: number
    }
  },
  getBookmarkCourseList: Function,
  removeCourseFromBookmarkList: Function
};

export default class CourseDetailsContext extends PureComponent<CourseDetailsContextProps> {
  render() {
    return (
      <UserSettingsConsumer>
        {({ toggleAuth }) => (
          // $FlowFixMe
          <CourseDetailsContainer {...this.props} toggleAuth={toggleAuth} />
        )}
      </UserSettingsConsumer>
    );
  }
}
