// @flow
import React, { PureComponent } from "react";

import Section, {
  SectionBody,
  SectionHeader
} from "../../../../../shared/components/Section";
import Row from "../../../../../shared/components/Row";
import Col from "../../../../../shared/components/Col";
import Course from "../../../../../components/Course";

import type { ConsolidatedCoursesListType } from "../../../../course/types";

type featuredCoursesProps = {
  featuredCourses: ConsolidatedCoursesListType,
  addCourseToBookmarkList: Function,
  removeCourseFromBookmarkList: Function
}

class FeaturedCourses extends PureComponent<featuredCoursesProps> {
  render() {
    const {
      featuredCourses,
      addCourseToBookmarkList,
      removeCourseFromBookmarkList
    } = this.props;
    const rendered = featuredCourses.map((item, index) => {
      return (
        <Col size="3" key={index}>
          <Course
            {...item}
            addCourseToBookmarkList={addCourseToBookmarkList}
            removeCourseFromBookmarkList={removeCourseFromBookmarkList}
          />
        </Col>
      );
    });
    return (
      featuredCourses.length > 0 && (
        <Section className="realated-courses">
          <SectionHeader position="left" heading="MAY INTEREST YOU" />
          <SectionBody>
            <Row>{rendered}</Row>
          </SectionBody>
        </Section>
      )
    );
  }
}

export default FeaturedCourses;