// @flow
import React, { PureComponent } from 'react';

import { connect } from "react-redux";

import Row from "../../../../shared/components/Row";
import Col from "../../../../shared/components/Col";
import Map from "../../../../shared/components/Map.jsx";
import Section, {
  SectionHeader
} from "../../../../shared/components/Section";
import Select from "../../../../shared/components/Select";

import PageHeader from "../../../../components/PageHeader";
import Sidebar, { Widget } from "../../../../components/Sidebar";
import ContactForm from "./components/ContactForm";
import { sendContactMessage } from "../../store/actions";

import "./styles.css";

type ContactusProps = {
  sendContactMessage: Function,
  isContactMessageSendSuccess: boolean,
  isLoading: boolean
}

class ContactUs extends PureComponent<ContactusProps> {

  HEADOFFICE_LATITUDE = -36.8618854;
  HEADOFFICE_LONGITUDE = 174.7622100;
  ARGENTINA_LATITUDE = -34.5914612;
  ARGENTINA_LONGITUDE = -58.4187304;

  render() {
    const {
      isContactMessageSendSuccess,
      sendContactMessage,
      isLoading
    } = this.props;

    return (
      <div>
        <PageHeader
          title="Contact tambook team"
          breadcrumbs={
            [
              {
                title: "CONTACT US"
              }
            ]
          }
        />
        <div className="contact-container">
          <Row>
            <Col size="8">
              <ContactForm
                sendContactMessage={sendContactMessage}
                isContactMessageSendSuccess={isContactMessageSendSuccess}
                isLoading={isLoading}
              />
            </Col>
            <Col size="4">
              <Sidebar>
                <Widget>
                  <div className="center-details">
                    <h4>TAMBOOK HEAD OFFICE</h4>
                    <div className="center-option">
                      <i className="fas fa-map-marker-alt" />
                      3 Glenside Crescent, Eden Terrace, Auckland, NZ.
                    </div>
                    <div className="center-option">
                      <i className="fas fa-envelope" />
                      office@tambook.com
                    </div>
                    <div className="center-option">
                      <i className="fas fa-clock" />
                      To schedule a meeting please send us an email
                    </div>
                  </div>
                </Widget>
                <Widget>
                  <div style={{ minHeight: "200px", width: "100%" }}>
                    <Map
                      lat={this.HEADOFFICE_LATITUDE}
                      lng={this.HEADOFFICE_LONGITUDE}
                    />
                  </div>
                </Widget>

              </Sidebar>
            </Col>
          </Row>
          <Row>
            <Col>
              <Section className="agent-filter-container">
                <SectionHeader
                  heading="Find your local Tambook agent"
                  position="left"
                />
                <div className="agent-filter">
                  <Select
                    options={["All Countries", "Argentina"]}
                    selected={"All Countries"}
                  />
                </div>
              </Section>
            </Col>
          </Row>
          <Row>
            <Col>
              <Section className="agent-detail-container">
                <Row>
                  <Col size="4">
                    <div className="center-details">
                      <h4>BUENOS AIRES - EVT PIN A MAP</h4>
                      <div className="center-option">
                        <i className="fas fa-map-marker-alt" />
                        Costa Rica 3975, CABA (1176), Argentina.
                      </div>
                      <div className="center-option">
                        <i className="fas fa-file-contract" />
                        License: 16620
                      </div>
                      <div className="center-option">
                        <i className="fas fa-envelope" />
                        latam‌@pamtravel.com
                      </div>
                      <div className="center-option">
                        <i className="fas fa-clock" />
                        To schedule a meeting please send us an email
                      </div>
                    </div>
                    <Widget>
                      <div style={{ minHeight: "200px", width: "100%" }}>
                        <Map
                          lat={this.ARGENTINA_LATITUDE}
                          lng={this.ARGENTINA_LONGITUDE}
                        />
                      </div>
                    </Widget>
                  </Col>
                  <Col size="4"></Col>
                  <Col size="4"></Col>
                </Row>
              </Section>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isContactMessageSendSuccess: state.general.isContactMessageSendSuccess,
    isLoading: state.general.isLoading
  };
};

const Actions = {
  sendContactMessage
};

export default connect(
  mapStateToProps,
  Actions
)(ContactUs);