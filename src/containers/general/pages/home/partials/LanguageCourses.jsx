import React, { PureComponent } from "react";
import { Link } from "react-router-dom";

import Row from "../../../../../shared/components/Row";
import Col from "../../../../../shared/components/Col";
import Section from "../../../../../shared/components/Section";

import europeImage from "../../../../../assets/home/europe.png";
import africaImage from "../../../../../assets/home/africa.png";
import asiaImage from "../../../../../assets/home/asia.png";
import australiaImage from "../../../../../assets/home/australia.png";
import northAmericaImage from "../../../../../assets/home/north-america.png";
import southAmericaImage from "../../../../../assets/home/south-america.png";

class LanguageCourses extends PureComponent {
  render() {
    return (
      <Section className="courses-all-around-section">
        <div className="container">
          <Row>
            <Col size="3">
              <p className="heading-3">LANGUAGE COURSES ALL AROUND THE WORLD</p>
            </Col>
            <Col>
              <Row>
                <Col className="country-card">
                  <Link to="/under-construction">
                    <img alt="EUROPE" src={europeImage} />
                    <span>EUROPE</span>
                  </Link>
                </Col>
                <Col className="country-card">
                  <Link to="/under-construction">
                    <img alt="ASIA" src={asiaImage} />
                    <span>ASIA</span>
                  </Link>
                </Col>
                <Col className="country-card">
                  <Link to="/under-construction">
                    <img alt="AFRICA" src={africaImage} />
                    <span>AFRICA</span>
                  </Link>
                </Col>
                <Col className="country-card">
                  <Link to="/under-construction">
                    <img alt="NORTH AMERICA" src={northAmericaImage} />
                    <span>NORTH AMERICA</span>
                  </Link>
                </Col>
                <Col className="country-card">
                  <Link to="/under-construction">
                    <img alt="SOUTH AMERICA" src={southAmericaImage} />
                    <span>SOUTH AMERICA</span>
                  </Link>
                </Col>
                <Col className="country-card">
                  <Link to="/under-construction">
                    <img alt="AUSTRALIA" src={australiaImage} />
                    <span>AUSTRALIA</span>
                  </Link>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </Section>
    );
  }
}

export default LanguageCourses;
