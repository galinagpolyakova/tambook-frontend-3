// @flow
import React, { PureComponent } from "react";
import Section, {
  SectionHeader,
  SectionBody
} from "../../../../../shared/components/Section";
import Col from "../../../../../shared/components/Col";
import Row from "../../../../../shared/components/Row";
import ReviewCard from "../components/ReviewCard";

import type { ReviewsListType } from "./../../../../review/types";

type WhatPeopleAreSayingProps = {
  featuredReviews: ReviewsListType
};

class WhatPeopleAreSaying extends PureComponent<WhatPeopleAreSayingProps> {
  getReviews(reviews: ReviewsListType) {
    if (reviews !== undefined && reviews.length > 0) {
      return reviews.map((review, key) => (
        <Col key={key}>
          <ReviewCard {...review} />
        </Col>
      ));
    }
  }
  render() {
    const { featuredReviews } = this.props;
    return (
      <Section className="bg-secondary what-people-saying-section">
        <div className="container">
          <SectionHeader heading="WHAT PEOPLE ARE SAYING" />
          <SectionBody>
            <Row>{this.getReviews(featuredReviews)}</Row>
          </SectionBody>
        </div>
      </Section>
    );
  }
}

export default WhatPeopleAreSaying;
