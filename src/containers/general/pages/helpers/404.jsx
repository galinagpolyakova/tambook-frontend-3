import React, { Component } from "react";

import Section from "../../../../shared/components/Section";

import notFound from "../../../../assets/404.svg";

import "./styles.css";

class UnderConstruction extends Component {
  render() {
    return (
      <div className="container">
        <Section className="helper-page">
          <img src={notFound} />
          <div className="heading-2">Oops!</div>
          <div className="heading-4">Page not found.</div>
        </Section>
      </div>
    );
  }
}

export default UnderConstruction;
