// import lib
import Loadable from "react-loadable";

// import components
import LoadingComponent from "../../components/Loader";

export default [
  {
    path: "/booking/course/:courseId",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/CourseInfo"),
      loading: LoadingComponent
    })
  },
  {
    path: "/booking/payment/:bookingId",
    exact: true,
    auth: true,
    component: Loadable({
      loader: () => import("./pages/Payment"),
      loading: LoadingComponent
    })
  }
];
