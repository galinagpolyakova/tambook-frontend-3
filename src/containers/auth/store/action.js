import {
  AUTH_INIT,
  AUTH_SIGNIN_SUCCESS,
  AUTH_FAILURE,
  AUTH_SIGNUP_SUCCESS,
  IS_USER_AUTHENTICATED_SUCCUSS,
  IS_USER_AUTHENTICATED_FAILURE,
  AUTH_SIGNOUT_SUCCESS,
  CLEAR_NOTIFICATIONS,
  AUTH_FORGOT_PASSWORD_SUCCESS,
  AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS,
  AUTH_CONFIRM_SIGN_UP_SUCCESS
} from "./actionTypes";

import { Auth } from "aws-amplify";
import { ROLES } from "../constants";
import { SITE_URL } from "../../../config/app";

Auth.configure({
  auth0: {
    domain: "tambook.au.auth0.com",
    clientID: "zIrPXSnI9nFKV459IJ7Ure8VYvzYQAbG",
    redirectUri: SITE_URL,
    responseType: "token id_token", // for now we only support implicit grant flow
    scope: "openid profile email", // the scope used by your app
    rredirectUri: SITE_URL
  }
});
function authInit() {
  return {
    type: AUTH_INIT
  };
}

function authSignInSuccess(payload) {
  return {
    type: AUTH_SIGNIN_SUCCESS,
    payload
  };
}

function authFailure(payload) {
  return {
    type: AUTH_FAILURE,
    payload
  };
}

export function authSignIn({ username, password }) {
  return (dispatch, getState, serviceManager) => {
    dispatch(authInit());
    Auth.signIn(username, password)
      .then(data => {
        serviceManager.get("ApiService").authToken =
          data.signInUserSession.idToken.jwtToken;
        dispatch(authSignInSuccess({ email: data.username }));
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function authSignUpSuccess(payload) {
  return {
    type: AUTH_SIGNUP_SUCCESS,
    payload
  };
}

export function authSignUp({ password, email, name }) {
  return dispatch => {
    dispatch(authInit());
    Auth.signUp({
      username: email,
      password,
      attributes: {
        email,
        name,
        "custom:role": ROLES.STUDENT
      }
    })
      .then(() => {
        dispatch(authSignUpSuccess());
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function isUserAuthenticatedSuccess(payload) {
  return {
    type: IS_USER_AUTHENTICATED_SUCCUSS,
    payload
  };
}

function isUserAuthenticatedFailure() {
  return {
    type: IS_USER_AUTHENTICATED_FAILURE
  };
}

export function isUserAuthenticated() {
  return (dispatch, getState, serviceManager) => {
    Auth.currentSession()
      .then(session => {
        serviceManager.get("ApiService").authToken = session.idToken.jwtToken;
        Auth.currentAuthenticatedUser().then(user => {
          dispatch(isUserAuthenticatedSuccess({ email: user.username }));
        });
      })
      .catch(error => {
        dispatch(isUserAuthenticatedFailure(error));
      });
  };
}

function authSignOutSuccess() {
  return { type: AUTH_SIGNOUT_SUCCESS };
}

export function authSignOut() {
  return dispatch => {
    Auth.signOut().then(dispatch(authSignOutSuccess()));
  };
}

export function clearNotifications() {
  return {
    type: CLEAR_NOTIFICATIONS
  };
}

export function federatedSignInSuccess() {
  return dispatch => {
    Auth.currentAuthenticatedUser().then(({ email }) => {
      dispatch(isUserAuthenticatedSuccess({ email }));
    });
  };
}

function authForgotPasswordSuccess(payload) {
  return {
    type: AUTH_FORGOT_PASSWORD_SUCCESS,
    payload
  };
}

export function authForgotPassword({ username }) {
  return dispatch => {
    dispatch(authInit());
    Auth.forgotPassword(username)
      .then(() => {
        dispatch(authForgotPasswordSuccess({ username }));
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function authforgotPasswordSubmitSuccess(payload) {
  return {
    type: AUTH_FORGOT_PASSWORD_SUBMIT_SUCCESS,
    payload
  };
}

export function authForgotPasswordSubmit({ username, code, new_password }) {
  return dispatch => {
    dispatch(authInit());
    Auth.forgotPasswordSubmit(username, code, new_password)
      .then(() => {
        dispatch(authforgotPasswordSubmitSuccess());
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}

function authConfirmSignUpSuccess() {
  return {
    type: AUTH_CONFIRM_SIGN_UP_SUCCESS
  };
}

export function authVerifyAccount({ username, code }) {
  return dispatch => {
    dispatch(authInit());
    Auth.confirmSignUp(username, code)
      .then(() => {
        dispatch(authConfirmSignUpSuccess());
      })
      .catch(error => {
        dispatch(authFailure({ error }));
      });
  };
}
