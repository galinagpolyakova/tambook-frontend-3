// @flow
import { type ApiServiceInterface } from "../../shared/services/ApiServiceInterface";

import type {
  ApiBlogType,
    BlogType
} from "./types";

import { Blog } from "./model";

export class BlogService {
  api: ApiServiceInterface;

  endpoint: string = "/blog";

  constructor(apiService: ApiServiceInterface) {
    this.api = apiService;
  }

  async request(endpoint: string, query: Object = {}, options: Object = {}) {
    let response = await this.api.get(endpoint, query, options);

    return response;
  }

  _normalizeCourse(apiBlog: ApiBlogType): BlogType {
    return Blog.fromApi(apiBlog);
  }

  getBlogPost(blogID: number): Promise<BlogType> {
    return this.api.get(`${this.endpoint}/${blogID}`).then(response => {
      return this._normalizeCourse(response.result);
    });
  }

  subscribeToBlog(payload: Object) {
    return this.api.post(`${this.endpoint}/subscribe`, payload);
  }
}